# This class is a simple redis install
class puppet_infra_profiles::redis::install (
  Boolean $manage_repo = true,
) {
  class { '::redis':
    manage_repo => $manage_repo,
  }
}

