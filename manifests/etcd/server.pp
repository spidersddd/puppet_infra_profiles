# This is to install etcd and configure hiera for it.
class puppet_infra_profiles::etcd::server (
) {
  class { 'etcd':
    config => {
      'data-dir'                    => '/var/lib/etcd',
      'name'                        => $facts['hostname'],
      'initial-advertise-peer-urls' => "https://${facts['networking']['ip']}:2380",
      'listen-peer-urls'            => "https://${facts['networking']['ip']}:2380",
      'listen-client-urls'          => "https://${facts['networking']['ip']}:2379",
      'advertise-client-urls'       => "https://${facts['networking']['ip']}:2379",
      'initial-cluster-token'       => 'etcd-cluster-1',
      'initial-cluster'             => "${facts['hostname']}=http://${facts['networking']['ip']}:2380",
      'initial-cluster-state'       => 'new',
      'client-transport-security'   => {
        'cert-file'        => "/etc/etcd/certs/${::clientcert}.pem",
        'key-file'         => "/etc/etcd/private_keys/${::clientcert}.pem",
        # authorize clients
        'client-cert-auth' => false,
        # and verify clients certificates
        'trusted-ca-file'  => '/etc/etcd/certs/ca.pem',
      },
      'peer-transport-security'     => {
        'cert-file'        => "/etc/etcd/certs/${::clientcert}.pem",
        'key-file'         => "/etc/etcd/private_keys/${::clientcert}.pem",
        # authorize clients
        'client-cert-auth' => false,
        # and verify clients certificates
        'trusted-ca-file'  => '/etc/etcd/certs/ca.pem',
      }
    },
  }
#  class { 'etcd':
#    ensure                      => 'latest',
#    etcd_name                   => $::hostname,
#    etcd_listen_client_urls          => 'https://0.0.0.0:2379',
#    etcd_advertise_client_urls       => "https://${::fqdn}:2379",
#    # clients should speak over ssl
#    etcd_cert_file                   => "/etc/etcd//certs/${::clientcert}.pem",
#    etcd_key_file                    => "/etc/etcd//private_keys/${::clientcert}.pem",
#    # authorize clients
#    etcd_client_cert_auth            => true,
#    # and verify clients certificates
#    etcd_trusted_ca_file             => "/etc/etcd//certs/ca.pem",
#    etcd_initial_cluster             => [
#        "${::hostname}=http://${::fqdn}:2380",
#        ],
#  }

  include etcd

  include baseline_profiles::firewall

  firewall { '101 puppet_infra_profiles::etcd::server rules':
    proto   => 'tcp',
    action  => 'accept',
    dport   => ['2379', '2380' ],
    require => Class['baseline_profiles::firewall'],
  }
}
