# Simple foreman install
class puppet_infra_profiles::foreman::server (
  String $init_admin_pass = 'SomesillyString',
  Boolean $postgresql = true,
) {
  include puppet_infra_profiles::puppet::onceover
  include puppet_infra_profiles::redis::install

  class { 'baseline_profiles::linux::centos::repos::foreman':
    require => Class['baseline_profiles::linux::centos::repos::centos_release_scl_rh', 'puppet_infra_profiles::redis::install'],
  }

  if $postgresql {
    require puppet_infra_profiles::puppet::postgres_server
  }

  class { 'foreman':
    rails_cache_store      => {
      'type'    => 'redis',
      'urls'    => ['localhost:8479/0'],
      'options' => {
        'compress'  => 'true',
        'namespace' => 'foreman'
      }
    },
    initial_admin_password => $init_admin_pass,
    require                => Foreman::Repos['foreman-2.4'],
  }

  include baseline_profiles::firewall

  firewall { '101 puppet_infra_profiles::foreman::server rules':
    proto   => 'tcp',
    action  => 'accept',
    dport   => ['80','443' ],
    require => Class['baseline_profiles::firewall'],
  }
}

