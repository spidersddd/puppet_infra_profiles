# This is to export the service to the haproxy balancer
class puppet_infra_profiles::puppet::balancermember (
    String $listening_service = 'puppet00',
    Variant[String, Array] $ports = '8140',
) {
  @@haproxy::balancermember { $facts['fqdn']:
    listening_service => $listening_service,
    ports             => $ports,
    server_names      => $facts['hostname'],
    ipaddresses       => $facts['ipaddress'],
    options           => 'check port 9140 inter 2000 rise 1 fall 1',
  }
}
