# This class will install onceover
class puppet_infra_profiles::puppet::onceover (
  Boolean $install_rh_ruby = false,
  Integer $ruby_major_version = 27,
) {
  if $install_rh_ruby {
    $gem_bin = "/opt/rh/rh-ruby${ruby_major_version}/root/bin/gem"
    class { 'baseline_profiles::linux::centos::rh_ruby':
      mversion => $ruby_major_version,
    }
  } else {
    $gem_bin =  '/opt/puppetlabs/puppet/bin/gem'
  }

  package { [ 'onceover', 'puppetlabs_spec_helper', ]:
    ensure   => installed,
    provider => gem,
    command  => $gem_bin,
  }
}
