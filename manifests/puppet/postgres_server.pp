# this si a profile for postgres install
class puppet_infra_profiles::puppet::postgres_server (
  String[3] $database_name = 'puppetdb',
  Boolean $manage_database = true,
  Array[String[6]] $listen_addresses = [ '127.0.0.1', ],
  String[3] $database_port = '5432',
  String[3] $database_username = 'puppetdb_user',
  String[3] $database_password = 'Th!s!sAS!llyPassw0rd',
) {

  class { 'postgresql::globals':
    encoding            => 'UTF-8',
    locale              => 'en_US.UTF-8',
    manage_package_repo => true,
    version             => '11',
    service_name        => 'postgresql-11',
    service_status      => 'systemctl status postgresql-11',
  }

  # get the pg server up and running
  class { '::postgresql::server':
    package_name            => 'postgresql11-server',
    ip_mask_allow_all_users => '0.0.0.0/0',
    datadir                 => '/var/lib/pgsql/11/data',
    service_name            => 'postgresql-11',
    listen_addresses        => $listen_addresses,
    port                    => scanf($database_port, '%i')[0],
  }

  # Only install pg_trgm extension, if database it is actually managed by the module
  if $manage_database {

    # get the pg contrib to use pg_trgm extension
    class { '::postgresql::server::contrib': }

    postgresql::server::extension { 'pg_trgm':
      database => $database_name,
      require  => Postgresql::Server::Db[$database_name],
    }
  }

  # create the puppetdb database
  postgresql::server::db { $database_name:
    user     => $database_username,
    password => $database_password,
    grant    => 'all',
  }

  include baseline_profiles::firewall

  firewall { '101 puppet_infra_profiles::puppet::postgres_server rules':
    proto   => 'tcp',
    action  => 'accept',
    dport   => [ $database_port ],
    require => Class['baseline_profiles::firewall'],
  }

}
