# this is to add required keys from metadata
class puppet_infra_profiles::puppet::lb_check_service (
  String $error_file = '/dev/null',
  String $service_to_check = 'master',
  ) {

  File {
    mode   => '0775',
    owner  => 'root',
    group  => 'nobody',
  }

  ::etc_services { 'puppet_check_service/tcp':
    port    => '9140',
    aliases => [ 'puppetmastercheck' ],
    comment => 'Puppet Master Check Service for haproxy'
  }

  class { 'xinetd':
    purge_confdir => true,
    require       => Etc_services['puppet_check_service/tcp'],
  }

  $check_script_location = '/usr/local/sbin/lb_check_service'

  file { $check_script_location:
    ensure  => file,
    content => epp('profiles/app/puppet/lb_check_service.sh.epp', {
      'error_file'       => $error_file,
      'service_to_check' => $service_to_check }),
  }

  xinetd::service { 'puppet_check_service':
    disable     => 'no',
    flags       => 'REUSE',
    socket_type => 'stream',
    port        => '9140',
    wait        => 'no',
    user        => 'nobody',
    server      => $check_script_location,
    log_type    => 'FILE /var/log/xinetdlog',
    only_from   => '0.0.0.0/0',
    per_source  => 'UNLIMITED',
    nice        => 19,
    require     => File[$check_script_location],
  }

  include baseline_profiles::firewall

  firewall { '101 puppet_infra_profiles::puppet::lb_check_service rules':
    proto   => 'tcp',
    action  => 'accept',
    dport   => ['9140' ],
    require => Class['baseline_profiles::firewall'],
  }

}
