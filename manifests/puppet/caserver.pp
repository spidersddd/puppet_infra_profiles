# Profile to build a caserver
class puppet_infra_profiles::puppet::caserver (
) {
#  include puppet_infra_profiles::puppet::hiera_setup
  if $facts['hypervisors']['kvm']['google'] {
    class { 'puppet_infra_profiles::puppet::gcp_keys':
      require => Class['::puppet'],
    }
  }

  include puppet_infra_profiles::puppet::server_configs

  class { 'baseline_profiles::selinux':
    mode => 'disabled',
  }

  class { 'puppetserver':
    config => {
      'java_args'     => {
        'xms' => '2g',
        'xmx' => '2g',
      },
    },
    before => Class['Puppetdb::Master::Puppetdb_conf', 'Puppetdb::Master::Report_processor', 'Puppetdb::Master::Storeconfigs'],
  }

  class { 'puppet_infra_profiles::puppet::catalog_diff':
    require => Class['puppetdb::master::puppetdb_conf', 'puppetdb::master::report_processor', 'puppetdb::master::storeconfigs'],
  }


  class { 'puppetdb::master::puppetdb_conf':
    server => $facts['fqdn'],
  }

  package { 'puppetdb-termini':
    ensure => installed,
    before => Class['Puppetdb::Master::Report_processor'],
  }

  include puppetdb::master::report_processor
  include puppetdb::master::storeconfigs
  include baseline_profiles::firewall

  firewall { '101 puppet_infra_profiles::puppet::server rules':
    proto   => 'tcp',
    action  => 'accept',
    dport   => ['8140' ],
    require => Class['baseline_profiles::firewall'],
  }

}

