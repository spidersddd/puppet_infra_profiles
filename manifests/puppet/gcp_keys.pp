# this is to add required keys from metadata
class puppet_infra_profiles::puppet::gcp_keys (
  String $control_repo_key = '/etc/puppetlabs/puppetserver/keys/control_repo_key',
  String $control_repo_pub = '/etc/puppetlabs/puppetserver/keys/control_repo_key.pub',
  String $private_key = '/etc/puppetlabs/puppetserver/keys/private_key.pkcs7.pem',
  String $public_key = '/etc/puppetlabs/puppetserver/keys/public_key.pkcs7.pem',
  String $directory = '/etc/puppetlabs/puppetserver/keys',
  String $control_repo = 'https://gitlab.com/spidersddd/some_roles_and_profiles.git',
) {
  File {
    mode   => '0700',
    owner  => 'puppet',
    group  => 'puppet',
  }

  file { $directory:
    ensure => directory,
  }

  exec { 'control_repo_key':
    creates => $control_repo_key,
    command => "/usr/bin/curl -s http://metadata.google.internal/computeMetadata/v1/instance/attributes/spri -H 'Metadata-Flavor: Google' > ${control_repo_key}",
  }

  exec { 'control_repo_pub':
    creates => $control_repo_pub,
    command => "/usr/bin/curl -s http://metadata.google.internal/computeMetadata/v1/instance/attributes/spub -H 'Metadata-Flavor: Google' > ${control_repo_pub}",
  }

  exec { 'private_key':
    creates => $private_key,
    command => "/usr/bin/curl -s http://metadata.google.internal/computeMetadata/v1/instance/attributes/private -H 'Metadata-Flavor: Google' > ${private_key}",
  #  refreshonly => true,
  }

  exec { 'public_key':
    creates => $public_key,
    command => "/usr/bin/curl -s http://metadata.google.internal/computeMetadata/v1/instance/attributes/public -H 'Metadata-Flavor: Google' > ${public_key}",
  }

  file { $control_repo_key:
    ensure    => file,
    subscribe => Exec['control_repo_key'],
  }

  file { $control_repo_pub:
    ensure    => file,
    mode      => '0644',
    subscribe => Exec['control_repo_pub'],
  }

  file { $private_key:
    ensure    => file,
    subscribe => Exec['private_key'],
  }

  file { $public_key:
    ensure    => file,
    mode      => '0644',
    subscribe => Exec['public_key'],
  }

  class { 'r10k':
    remote   => $control_repo,
    provider => 'puppet_gem',
  }
}
