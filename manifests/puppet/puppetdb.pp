#This class will configure puppetdb for local puppetserver
class puppet_infra_profiles::puppet::puppetdb (
  String[3] $mastername = $trusted['certname'],
  String[3] $database_name = 'puppetdb',
  Boolean $manage_database = false,
  Array[String[6]] $listen_addresses = [ '127.0.0.1', ],
  String[3] $database_host = "postgresql-server.${facts['domain']}",
  String[3] $database_port = '5432',
  String[3] $database_username = 'puppetdb_user',
  String[3] $database_password = 'Th!s!sAS!llyPassw0rd',
  Integer $puppetdb_listen_port = 8081,
  Array $certificate_whitelist = [ $trusted['certname'], "ca-server.${facts['domain']}", ],
) {
  # Here we install and configure PuppetDB, and tell it where to
  # find the PostgreSQL database.
  class { 'puppetdb::server':
    disable_ssl            => false,
    ssl_dir                => '/etc/puppetlabs/puppetdb/ssl',
    database_host          => $database_host,
    database_username      => $database_username,
    database_password      => $database_password,
    read_database_password => $database_password,
    read_database_username => $database_username,
    certificate_whitelist  => $certificate_whitelist,
  }

  # Only install pg_trgm extension, if database it is actually managed by the module
  if $manage_database {

    # get the pg contrib to use pg_trgm extension
    class { '::postgresql::server::contrib': }

    postgresql::server::extension { 'pg_trgm':
      database => $database_name,
    }
  }

  include baseline_profiles::firewall

  firewall { '101 puppet_infra_profiles::puppet::puppetdb rules':
    proto   => 'tcp',
    action  => 'accept',
    dport   => [ '8081' ],
    require => Class['baseline_profiles::firewall'],
  }

#  firewall { '101 puppet_infra_profiles::puppet::postgres_server rules':
#    proto   => 'tcp',
#    action  => 'accept',
#    dport   => [ $database_port ],
#    require => Class['baseline_profiles::firewall'],
#  }

}
