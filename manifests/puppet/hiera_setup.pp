# profile for hiera setup
class puppet_infra_profiles::puppet::hiera_setup (
  Boolean $etcd_local = false,
) {
  if $etcd_local {
    include puppet_infra_profiles::etcd::server
  }

  class { 'hiera':
    eyaml           => true,
    hiera_version   =>  '5',
    hiera5_defaults =>  {
      'datadir'    => 'data',
      'lookup_key' => 'eyaml_lookup_key',
      'options'    => {
        'pkcs7_private_key' => '/etc/puppetlabs/puppet/keys/private_key.pkcs7.pem',
        'pkcs7_public_key'  => '/etc/puppetlabs/puppet/keys/public_key.pkcs7.pem',
      },
    },
    hierarchy       =>  [
      {'name' =>  'Per-node trusted.certname data (can be encrypted)', path =>  'nodes/%{trusted.certname}.yaml'},
      {'name' =>  'Per-datacenter buiness trusted.extentions.pp_role data (can be encrypted)', path =>  'datacenter/%{facts.datacenter}/%{trusted.extensions.pp_role}.yaml'}, # lint:ignore:140chars
      {'name' =>  'Role trusted.extentions.pp_role data (can be encrypted)', path =>  'role/%{trusted.extensions.pp_role}.yaml'},
      {'name' =>  'Per-OS facts.os.family defaults data (can be encrypted)', path =>  'os/%{facts.os.family}.yaml'},
      {'name' =>  'Virtual facts.virtual defaults', path =>  'virtual/%{facts.virtual}.yaml'},
      {'name' =>  'Common data (can be encrypted)', path =>  'common.yaml'},
    ],
  }
}
