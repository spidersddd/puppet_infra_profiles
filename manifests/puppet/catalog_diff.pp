# Profile to install catalog diff tooling
class puppet_infra_profiles::puppet::catalog_diff (
) {
  $dev_packages = [
    'autoconf', 'automake', 'binutils', 'bison', 'flex', 'gcc', 'gcc-c++',
    'gettext', 'libtool', 'make', 'patch', 'pkgconfig', 'redhat-rpm-config', 'rpm-build',
    'rpm-sign', 'byacc', 'cscope', 'ctags', 'diffstat', 'doxygen', 'elfutils', 'gcc-gfortran',
    'git', 'indent', 'intltool', 'patchutils', 'rcs', 'subversion', 'swig', 'systemtap', 'openssl',
    'openssl-devel', 'libcurl', 'libcurl-devel', 'wget', 'rh-ruby24', 'rh-ruby24-ruby-devel',
    'rh-ruby24-scldevel.x86_64'
  ]

  include puppet_infra_profiles::install_cmake

  package { 'centos-release-scl':
    ensure => installed,
    before => Package[$dev_packages],
  }

  file { '/opt/rh/rh-ruby24/root/usr/libexec/scl_wrapper':
    ensure  => file,
    mode    => '0755',
    source  => 'puppet:///modules/profiles/app/puppet/scl_wrapper',
    require => Package[$dev_packages],
    before  => File['/usr/local/bin/gem'],
  }

  package { $dev_packages:
    ensure => installed,
    before => Class['puppet_infra_profiles::install_cmake'],
  }

  file { '/usr/local/bin/gem':
    ensure  => link,
    target  => '/opt/rh/rh-ruby24/root/usr/libexec/scl_wrapper',
    require => File['/opt/rh/rh-ruby24/root/usr/libexec/scl_wrapper'],
  }

  package { 'puppet':
    ensure   => '5.5.22',
    provider => gem,
    require  => File['/usr/local/bin/gem'],
  }

  package { 'parallel':
    ensure   => '1.19.2',
    provider => gem,
    require  => File['/usr/local/bin/gem'],
  }

  package { [ 'onceover', 'octocatalog-diff', 'onceover-octocatalog-diff' ]:
    ensure   => installed,
    provider => gem,
    require  => [ File['/usr/local/bin/gem'], Package['parallel'], Package['puppet'] ],
  }
}
