# Profile to do lookup to hiera and include the classes.
class puppet_infra_profiles::puppet::hiera_included (
  Array $include_profiles = [],
) {
  $_profiles_to_include = $included_profiles.filter | $class_name | { $class_name =~ /_profiles::/ }

  include $_profiles_to_include
}
