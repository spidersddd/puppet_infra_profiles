# @summary
#   This class can be used to enforce site specific settings on all the Puppet Enterprise Masters/Compilers.
#
# @note
#   This profile will check to see the master is running the CA service and include 
#   specific profiles relating to compiler and master of master configurations.
#
# @param puppet_ca 
#   This would be the Puppet CA fqdn.
#
# @param debug_messages 
#   This will enable or disable some notifications for debuging this profile
#
class puppet_infra_profiles::puppet::master (
  Optional[String] $puppet_ca = undef,
  Boolean $debug_messages = false,
) {
  if $trusted['extensions']['pp_role'] == 'puppetserver' {

    # This will quiery the puppet_db to see what hosts are running as the Puppet CA.
    $puppetdb_puppet_ca = 'resources[certname] { type = "Class" and title = "Profiles::App::Puppet::Certificate_authority" }'
    $puppet_ca_nodes = puppetdb_query($puppetdb_puppet_ca).each |$value| { $value["certname"] }
    # This will check if puppet_ca param was assigned and if not use
    if $puppet_ca == undef {
      $puppetserver_ca = $puppet::certificate_authority_host
    } else {
      $puppetserver_ca = $puppet_ca
    }

    # This code will include a class on compilers but not on the puppet_ca (Master of Masters)
    if (! $trusted['certname'] in $puppet_ca_nodes) and (! $trusted['certname'] == $puppetserver_ca) {
      include puppet_infra_profiles::puppet::compiler
      if $debug_messages {
        notify { 'Compiler message':
          message => "${facts['fqdn']} is NOT running the Puppet_enterprise::Profiles::Certificate_authority class",
        }
      }
    } else {
      # This is a Master of Masters section to add classes to
      if $debug_messages {
        notify { 'Master message':
          message => "${facts['fqdn']} is running the Puppet_enterprise::Profiles::Certificate_authority class",
        }
      }
    }
  }
#  include puppet_infra_profiles::puppet::check_hiera_keys

}

