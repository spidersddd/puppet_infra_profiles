# Profile to install puppet server
class puppet_infra_profiles::puppet::server (
  Boolean $foreman = false,
  String $foreman_external_nodes = '',
  String $server_reports = 'store',
  String[3] $puppetdb_host = "puppetdb-server.${facts['domain']}",
) {
  case $facts['productname'] {
    'Google Compute Engine': {
      class { 'puppet_infra_profiles::puppet::gcp_keys':
        require => Class['::puppet'],
      }
    }
    default: { }
  }

  include baseline_profiles::linux::time_client
  include puppet_infra_profiles::puppet::server_configs

  # Agent and puppetmaster:
  if $server_reports =~ /puppetdb/ {
    class { '::puppet':
      server                => true,
      server_foreman        => $foreman,
      server_reports        => $server_reports,
      server_external_nodes => $foreman_external_nodes,
      server_storeconfigs   => true,
    }
    class { 'puppet::server::puppetdb':
      server => $puppetdb_host,
    }
  } else {
    class { '::puppet':
      server                => true,
      server_foreman        => $foreman,
      server_reports        => $server_reports,
      server_external_nodes => $foreman_external_nodes,
    }
  }

  if $foreman {
    include puppet_infra_profiles::foreman::server
  }

  include puppet_infra_profiles::puppet::onceover
  include baseline_profiles::firewall

  firewall { '101 puppet_infra_profiles::puppet::server rules':
    proto   => 'tcp',
    action  => 'accept',
    dport   => ['8140' ],
    require => Class['baseline_profiles::firewall'],
  }
}
